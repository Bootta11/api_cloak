let table_name = 'transactions';

exports.up = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(!exists){
            return knex.schema.createTable(table_name, function (table) {
                table.increments('id');
                table.timestamp('creation_date').notNull();
                table.integer('consumer').notNull();
                table.integer('provider').notNull();

                table.foreign('consumer').references('consumers.id')
                table.foreign('provider').references('providers.id')
            });
        }
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(exists){
            return knex.schema.dropTable(table_name);
        }
    });
};
