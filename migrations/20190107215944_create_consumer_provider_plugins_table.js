let table_name = 'consumer_provider_plugins';

exports.up = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(!exists){
            return knex.schema.createTable(table_name, function (table) {
                table.increments('id');
                table.string('name', 200).notNull();
                table.string('phase', 50).notNull();
                table.text('settings').notNull();
                table.integer('priority').notNull();
                table.integer('consumer_provider').notNull();

                table.foreign('consumer_provider').references('consumer_providers.id')
            });
        }
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(exists){
            return knex.schema.dropTable(table_name);
        }
    });
};
