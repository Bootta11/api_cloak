let table_name = 'fields_mappings';

exports.up = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(!exists){
            return knex.schema.createTable(table_name, function (table) {
                table.increments('id');
                table.string('field_name', 100).notNull();
                table.string('mapping', 100).notNull();
                table.string('type',20).notNull();
                table.integer('consumer_provider').notNull();

                table.foreign('consumer_provider').references('consumer_providers.id')
            });
        }
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(exists){
            return knex.schema.dropTable(table_name);
        }
    });
};