let table_name = 'providers';

exports.up = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(!exists){
            return knex.schema.createTable(table_name, function (table) {
                table.increments('id');
                table.string('name',100).notNull();
                table.string('url',2000).notNull();
                table.string('request_type',20).notNull();
                table.string('content_type',50).notNull();
                table.integer('timeout').notNull();
                table.boolean('strict').notNull().defaultTo(false);
            });
        }
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(exists){
            return knex.schema.dropTable(table_name);
        }
    });
};
