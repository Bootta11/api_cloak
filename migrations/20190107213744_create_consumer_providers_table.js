let table_name = 'consumer_providers';

exports.up = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(!exists){
            return knex.schema.createTable(table_name, function (table) {
                table.increments('id');
                table.integer('consumer').notNull();
                table.integer('provider').notNull();
                table.string('response_content_type',50).notNull();

                table.foreign('consumer').references('consumers.id')
                table.foreign('provider').references('providers.id')
            });
        }
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(exists){
            return knex.schema.dropTable(table_name);
        }
    });
};
