let table_name = 'consumer_transaction_fields';

exports.up = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(!exists){
            return knex.schema.createTable(table_name, function (table) {
                table.increments('id');
                table.integer('transaction').notNull();
                table.string('field_name', 100).notNull();
                table.string('field_value', 1000).notNull();

                table.foreign('transaction').references('transactions.id')
            });
        }
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.hasTable(table_name).then(function(exists){
        if(exists){
            return knex.schema.dropTable(table_name);
        }
    });
};
