const admin_config = require('./config/admin_app.config.js');
const admin_config_validation_schema = require('./config/admin_app.schema.js');
const config_validator = require('./modules/config_validator/config_validator.js');
config_validator.validateConfigFile(admin_config, admin_config_validation_schema);

const express = require('express');
const admin_app = express();
const simplog = require('./modules/simplog/simplog.js');
const body_parser = require('body-parser');
const admin_log = new simplog(admin_config.log_level, 'admin_server_js');
const package_info = require('./package.json');
const cors = require('cors');
const db = require('./modules/database/db.js');

let corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

admin_app.use(body_parser.json()); // support json encoded bodies
admin_app.use(body_parser.urlencoded({extended: true})); // support encoded bodies
admin_app.use(cors(corsOptions));

//admin routers
const ADMIN_BASE_PATH = '/api/' + package_info.version + '/admin/';

const ADMIN_PROVIDERS_ROUTER = require('./modules/admin/providers/index.js');
const ADMIN_CONSUMER_PROVIDERS_ROUTER = require('./modules/admin/consumer_providers/index.js');
const ADMIN_CONSUMERS_ROUTER = require('./modules/admin/consumers/index.js');
const ADMIN_FIELDS_MAPPINGS_ROUTER = require('./modules/admin/fields_mappings/index.js');

admin_app.use(ADMIN_BASE_PATH, ADMIN_PROVIDERS_ROUTER);
admin_app.use(ADMIN_BASE_PATH, ADMIN_CONSUMER_PROVIDERS_ROUTER);
admin_app.use(ADMIN_BASE_PATH, ADMIN_CONSUMERS_ROUTER);
admin_app.use(ADMIN_BASE_PATH, ADMIN_FIELDS_MAPPINGS_ROUTER);

let admin_server = admin_app.listen(admin_config.port, admin_config.host, function () {
    admin_log.info('API cloak admin_app listening on port ' + admin_config.host + ':' + admin_config.port + ', Env: ' + admin_app.get('env') + ', Version: ' + package_info.version);

    db.runDbCheck(function (err) {
        if (err) {
            throw err;
        }
    });
});

module.exports = admin_server;