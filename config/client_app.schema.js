// client app config validation schema

const Joi = require('joi');

const schema = Joi.object().keys({
    'host': Joi.string().required(),
    'port': Joi.number().integer().greater(1024).required(),
    'log_level': Joi.string().required()
});

module.exports = schema;

