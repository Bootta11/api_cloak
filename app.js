const simplog = require('./modules/simplog/simplog.js');
const client_log = new simplog('details', 'app_js');
const plugins = require('./modules/plugins/plugins.js');

process.on('unhandledRejection', (unhandled_rejection) => {
    throw unhandled_rejection;
});

module.exports = {
    client_server: require('./client.server.js'),
    admin_server: require('./admin.server.js')
};