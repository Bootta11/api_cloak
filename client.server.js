const client_config = require('./config/client_app.config.js');
const client_config_validation_schema = require('./config/client_app.schema.js');
const config_validator = require('./modules/config_validator/config_validator.js');
config_validator.validateConfigFile(client_config, client_config_validation_schema);

const express = require('express');
const client_app = express();
const simplog = require('./modules/simplog/simplog.js');
const body_parser = require('body-parser');
const client_log = new simplog(client_config.log_level, 'client_server_js');
const package_info = require('./package.json');
const cors = require('cors');
var helmet = require('helmet');
const db = require('./modules/database/db.js');

let corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

client_app.use(body_parser.json()); // support json encoded bodies
client_app.use(body_parser.urlencoded({extended: true})); // support encoded bodies
client_app.use(cors(corsOptions));
client_app.use(helmet())

//client routers
const CLIENT_BASE_PATH = '/api/' + package_info.version + '/client/';

const CLIENT_PROVIDERS_ROUTER = require('./modules/client/providers/index.js');

client_app.use(CLIENT_BASE_PATH, CLIENT_PROVIDERS_ROUTER);

let client_server = client_app.listen(client_config.port, client_config.host, function () {
    client_log.info('API cloak client_app listening on port ' + client_config.host + ':' + client_config.port + ', Env: ' + client_app.get('env') + ', Version: ' + package_info.version);

    db.runDbCheck(function (err) {
        if (err) {
            throw err;
        }
    });
});

module.exports = client_server;