
exports.seed = function(knex, Promise) {
    let table_name = 'consumer_providers';
    // Deletes ALL existing entries
    return knex(table_name).del()
        .then(function () {
            // Inserts seed entries
            return knex(table_name).insert([
                {id: 1, consumer: 1, provider: 3, response_content_type: 'application/json'},
                {id: 2, consumer: 2, provider: 2, response_content_type: 'application/json'},
                {id: 3, consumer: 3, provider: 1, response_content_type: 'application/json'},
            ]);
        });
};
