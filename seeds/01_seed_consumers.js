
exports.seed = function(knex, Promise) {
    let table_name = 'consumers';
  // Deletes ALL existing entries
  return knex(table_name).del()
    .then(function () {
      // Inserts seed entries
      return knex(table_name).insert([
        {id: 1, name: 'consumer1', api_key: 'apikey1'},
        {id: 2, name: 'consumer2', api_key: 'apikey2'},
        {id: 3, name: 'consumer3', api_key: 'apikey3'},
      ]);
    });
};
