
exports.seed = function(knex, Promise) {
    let table_name = 'consumer_provider_plugins';
    // Deletes ALL existing entries
    return knex(table_name).del()
        .then(function () {
            // Inserts seed entries
            return knex(table_name).insert([
                {
                    id: 1,
                    name: 'shout',
                    phase: 'finish',
                    settings: {
                        msg: 'Msg from Shout plugin #1'
                    },
                    priority: 1,
                    consumer_provider: 1
                },
                {
                    id: 2,
                    name: 'shout',
                    phase: 'finish',
                    settings: {
                        msg: 'Msg from Shout plugin #2'
                    },
                    priority: 1,
                    consumer_provider: 2
                },
                {
                    id: 3,
                    name: 'shout',
                    phase: 'finish',
                    settings: {
                        msg: 'Msg from Shout plugin #3'
                    },
                    priority: 1,
                    consumer_provider: 3
                },
            ]);
        });
};
