
exports.seed = function(knex, Promise) {
    let table_name = 'providers';
    // Deletes ALL existing entries
    return knex(table_name).del()
        .then(function () {
            // Inserts seed entries
            return knex(table_name).insert([
                {id: 1,
                    name: 'provider1',
                    url: 'https://postman-echo.com/get?foo1=bar1&foo=2',
                    request_type: 'GET',
                    content_type: 'application/x-www-form-urlencoded',
                    timeout: '30000',
                    strict: false,
                },
                {id: 2,
                    name: 'provider2',
                    url: 'https://postman-echo.com/get?foo1=bar1&foo=2',
                    request_type: 'GET',
                    content_type: 'application/x-www-form-urlencoded',
                    timeout: '30000',
                    strict: false,
                },
                {id: 3,
                    name: 'provider3',
                    url: 'https://postman-echo.com/get?foo1=bar1&foo=2',
                    request_type: 'GET',
                    content_type: 'application/x-www-form-urlencoded',
                    timeout: '30000',
                    strict: false,
                },

            ]);
        });
};