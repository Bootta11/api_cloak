const path = require('path');

const paths = {
    DIST: path.resolve(__dirname, 'dist'),
    SRC: path.resolve(__dirname, 'src'),
    JS: path.resolve(__dirname, 'src/js')
};

module.exports = {
    entry: path.join(paths.JS, 'app.js')  
};