const fs = require('fs');
const path = require('path');

function get_dir_list(path) {
    return fs.readdirSync(path).filter(function (f) {
        //console.log(f);
        return fs.statSync(path + '/' + f).isDirectory();
    });
};

function getPluginDirs() {
    let all_dirs = get_dir_list(path.join(__dirname, '/../../node_modules'));

    return plugin_dirs = all_dirs.filter(function(dir){
        return /\.apicloak.plugin$/.test(dir) && dir!=='base.apicloak.plugin';
    });
}

function getPlugins(){
    let plugins={};
    
    let plugin_dirs=getPluginDirs();
    
    for(let i=0;i<plugin_dirs.length;i++){
        let plugin = require('../../node_modules/'+plugin_dirs[i])();
        plugins[plugin.name] = plugin;
    }
    
    return plugins;
}


module.exports = {
    get:getPlugins,
    dirs_list: get_dir_list,
};