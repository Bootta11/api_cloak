var winston = require('winston');


// Simple log
module.exports = Simplog;
function Simplog(ll, log_prefix = '') {
    this.log_level = ll;
    this.prefix = log_prefix;
}

Simplog.prototype.details = details;
function details() {
    var log_level_param = 'verbose';
    if (this.log_level && (this.log_level == 'details')) {
        this.HandleArgumentListAndLogToConsole(arguments, this.prefix, log_level_param);
    }
}

Simplog.prototype.info = info;
function info() {
    var log_level_param = 'info';
    if (this.log_level && (this.log_level == 'details' || this.log_level == 'info')) {
        this.HandleArgumentListAndLogToConsole(arguments, this.prefix, log_level_param);
    }
}

Simplog.prototype.warn = warn;
function warn() {
    var log_level_param = 'warn';
    if (this.log_level && (this.log_level == 'details' || this.log_level == 'info' || this.log_level == 'warn')) {
        this.HandleArgumentListAndLogToConsole(arguments, this.prefix, log_level_param);
    }
}

Simplog.prototype.error = error;
function error() {
    var log_level_param = 'error';
    if (this.log_level && (this.log_level == 'details' || this.log_level == 'info' || this.log_level == 'warn' || this.log_level == 'error')) {
        this.HandleArgumentListAndLogToConsole(arguments, this.prefix, log_level_param);
    }
}

Simplog.prototype.HandleArgumentListAndLogToConsole = HandleArgumentListAndLogToConsole;
function HandleArgumentListAndLogToConsole(log_arguments, prefix, log_level_param) {
    var log_prefix_label = (prefix ? '[' + prefix + '] ' : '');

    if (log_arguments.length > 0) {
        if (log_arguments.length > 1) {
            var obj;
            try {
                obj = JSON.parse(log_arguments[1]);
            } catch (e) {
                obj = log_arguments[1];
            }
            winston.log(log_level_param, log_prefix_label + log_arguments[0], obj);
        } else {
            if (log_arguments[0] instanceof Error) {
                let error = log_arguments[0];
                
                winston.log(log_level_param, log_prefix_label + '('+error.message+')'+ '\nStack trace: '+error.stack);
            } else {
                winston.log(log_level_param, log_prefix_label + log_arguments[0]);
            }
        }
    }
}