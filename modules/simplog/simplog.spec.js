const Simplog = require('./simplog.js');


describe('Simplog', () => {
    let log_name = 'test_log';
    let input_data;

    
    function getLog(log_level, log_name){
        let new_log = new Simplog(log_level, log_name);
        input_data = undefined;
        new_log.HandleArgumentListAndLogToConsole = jest.fn(function () {
            input_data = arguments;
        });
        return new_log;
    }


    describe('log', () => {
        it('should be object', () => {
            let log_with_name = new Simplog('details', log_name);
            expect(typeof log_with_name).toBe('object');
            let log_without_name = new Simplog('details');
            expect(typeof log_without_name).toBe('object');
        });
    });


    describe('log.details', () => {

        it('should write message to log', () => {
            let msg = 'test detail';
            let log = getLog('details',log_name);
            log.details(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('unknown level', log_name);
            log.details(msg);
            expect(input_data).toBeUndefined();
        });
    });

    describe('log.info', () => {

        it('should write message to log', () => {
            let msg = 'test info';
            let log = getLog('details', log_name);
            log.info(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('info', log_name);
            log.info(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('unknown level', log_name);
            log.info(msg);
            expect(input_data).toBeUndefined();
        });
    });

    describe('log.warn', () => {

        it('should write message to log', () => {
            let msg = 'test warn';
            let log = getLog('details', log_name);
            log.warn(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('info', log_name);
            log.warn(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('warn', log_name);
            log.warn(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('unknown level', log_name);
            log.warn(msg);
            expect(input_data).toBeUndefined();
        });
    });

    describe('log.error', () => {

        it('should write message to log', () => {
            let msg = 'test error';
            let log = getLog('details', log_name);
            log.error(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('info', log_name);
            log.error(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('warn', log_name);
            log.error(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('error', log_name);
            log.error(msg);
            expect(input_data[0][0]).toBe(msg);
            log = getLog('unknown level', log_name);
            log.error(msg);
            expect(input_data).toBeUndefined();
        });
    });

    
    describe('HandleArgumentListAndLogToConsole', () => {
        
        
        it('should not return error', () => {
            let log = new Simplog('details', log_name);
            let err=null;
            try{
                log.HandleArgumentListAndLogToConsole([], '', 'verbose');
                log.HandleArgumentListAndLogToConsole(['test msg'], '', 'verbose');
                log.HandleArgumentListAndLogToConsole(['test msg'],log_name,'verbose');
                log.HandleArgumentListAndLogToConsole(['test msg1', 'test msg2'], log_name, 'verbose');
                log.HandleArgumentListAndLogToConsole([new Error('Test error message')], log_name, 'verbose');
            }catch(ex){
                err=ex;
            }

            expect(err).toBeNull();
        });
        
    });
    

});
