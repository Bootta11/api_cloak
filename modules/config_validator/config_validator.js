const Joi = require('joi');

function validateConfigFile(config, config_validation_schema) {

    const config_validation_result = Joi.validate(config, config_validation_schema);
    if (config_validation_result.error) {
        const error_details = config_validation_result.error.details;

        let validation_errors_list = [];
        error_details.forEach(function (detail) {
            validation_errors_list.push(detail);
        });


        throw new Error("Application config is not valid. Validation error: " + validation_errors_list.toString());
    } else {
        return config_validation_result.value;
    }
}

module.exports = {
    validateConfigFile: validateConfigFile
};