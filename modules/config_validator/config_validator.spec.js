const validator = require('./config_validator.js');
const Joi = require('joi');

const schema = Joi.object().keys({
    'host': Joi.string().required(),
    'port': Joi.number().integer().greater(1024).required(),
    'log_level': Joi.string().required()
});

const config = {
    host: 'localhost',
    port: 3008,
    log_level: 'details'
};

describe('Config validator', () => {
    
    describe('method validateConfigFile', () => {
        
        it('should return same config JSON if config is valid', () => {
            const result=validator.validateConfigFile(config,schema);
            expect(result).toEqual(config);
        });

        it('should return Error if config is not valid', () => {
            config.host = undefined;
            config.port ='-1024';
            let err=null;
            try{
            const result = validator.validateConfigFile(config, schema);
            }catch(ex){
                err=ex;
            }
            expect(err!==null).toBe(true);
            expect(err instanceof Error).toBe(true);
            err=null;
            config.port = 5555;
            try {
                const result = validator.validateConfigFile(config, schema);
            } catch (ex) {
                err = ex;
            }
            expect(err !== null).toBe(true);
            expect(err instanceof Error).toBe(true);
        });
        
    });
    
});
