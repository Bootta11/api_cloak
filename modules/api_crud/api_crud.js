module.exports = function (db, table_name, table_display_name, table_fields, log) {
    function requestRead(req, res) {
        let filter = {};

        if (req.params && req.params.id) {
            filter.id = req.params.id;
        } else if (req.query && Object.keys(req.query).length > 0) {
            let query_keys = Object.keys(req.query);
            let pick_only_filter_parameters = query_keys.filter(function (qparam) {
                if (qparam !== 'sort') {
                    filter[qparam] = req.query[qparam];
                    return true;
                }
                return false;
            });
        } else {
            return res.status(400).send({ error: 'Wrong or missing parameters' });
        }
        let order_by = {};
        console.log('query: ', req.query)
        if (req.query && req.query.sort) {
            let order_value = 'ASC';
            let order_name = req.query.sort;
            if (order_name[0] === '+') {
                order_name = order_name.slice(1);
            } else if (order_name[0] === '-') {
                order_name = order_name.slice(1);
                order_value = 'DESC';
            }else{
                order_name = order_name;
            }

            order_by[order_name] = order_value;
        }

        db.select(filter, order_by, table_name, function (err, result) {
            console.log('err: ', err, ' result: ', result);
            if (err) {
                return res.send({ error: err });
            }
            res.send({ result: result });
        });
    }

    function requestAdd(req, res) {
        console.log(res.body);
        if (!req.body || Object.keys(req.body).length == 0 || !table_fields.every(function (param) {
            console.log('el: ', param);
            return Object.keys(req.body).indexOf(param) > -1;
        })) {
            return res.status(400).send({ error: 'Wrong or missing parameters' });
        }

        let entry = {};
        for (let key in table_fields) {
            entry[table_fields[key]] = req.body[table_fields[key]];
        }

        console.log('entry: ', entry);

        db.insert([entry], table_name, function (err, result) {
            console.log('insert err: ', err, ' result: ', result);
            if (err) {
                return res.send({ error: err });
            }
            res.send({ result: result });
        });

    }

    function requestDelete(req, res) {
        console.log(res.body);
        if (!req.params || !(Object.keys(req.params).length > 0)) {
            return res.status(400).send({ error: 'Request missing select parameter' });
        }

        let key = Object.keys(req.params)[0];
        let filter = {};
        filter[key] = req.params[key];

        db.delete(filter, table_name, function (err, result) {
            console.log('err: ', err, ' result: ', result);
            if (err) {
                return res.send({ error: err });
            }
            res.send({ result: table_display_name + ' deleted' });
        });
    }

    function requestUpdate(req, res) {
        if (!req.params || !(Object.keys(req.params).length > 0)) {
            return res.status(400).send({ error: 'Request missing select parameter' });
        }

        let key = Object.keys(req.params)[0];
        let filter = {};
        filter[key] = req.params[key];

        
        if (!req.body || Object.keys(req.body).length == 0 || !Object.keys(req.body).every(function (param) {
            console.log('el: ', param);
            return table_fields.indexOf(param) > -1;
        })) {
            return res.status(400).send({ error: 'Wrong or missing parameters' });
        }

        let updated_entry = {};
        for (let key in req.body) {
            updated_entry[key] = req.body[key];
        }



        db.update(filter, updated_entry, table_name, function (err, result) {
            console.log('err: ', err, ' result: ', result);
            if (err) {
                return res.send({ error: err });
            }
            res.send({ result: table_display_name + ' updated' });
        });
    }



    return {
        requestRead: requestRead,
        requestAdd: requestAdd,
        requestDelete: requestDelete,
        requestUpdate: requestUpdate
    };
};