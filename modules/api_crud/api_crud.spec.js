const api_crud = require('./api_crud.js');
let db = {
    select: jest.fn(function (filter, order_by, table_name, callback) {
        callback(null, [])
    }),
    insert: jest.fn(function (entries, table_name, callback){
        callback(null,{})
    }),
    delete: jest.fn(function (filter, table_name, callback) {
        callback(null, {})
    }),
    update: jest.fn(function (filter,updated_entry, table_name, callback) {
        callback(null, {})
    })
}

const log = {

}
const crud = api_crud(db, 'test_table', 'test_table_display', ['field1'], log);
let res;

describe('Api crud', () => {
    beforeEach(function(){
        res = {
            send: jest.fn(),
            status: jest.fn()
        };
        res.send.mockImplementation(()=>res);
        res.status.mockImplementation(() => res);
    });

    describe('method', () => {

        describe('requestRead', () => {


            describe('with id field in params', () => {
                it('should return array with result field', () => {
                    let spy = jest.spyOn(crud, 'requestRead');
                    let req = {
                        params: { id: "1" }
                    };
                    

                    crud.requestRead(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('result')).toBe(true);
                });
            });

            describe('with query params and asc sort param', () => {
                it('should return array with result field', () => {
                    let spy = jest.spyOn(crud, 'requestRead');
                    let req = {
                        query: {
                            "field1": "field_value", 
                            "sort": "+field"
                        }
                    };
                    

                    crud.requestRead(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('result')).toBe(true);
                });
            });

            describe('with empty query params', () => {
                it('should return array with error', () => {
                    let spy = jest.spyOn(crud, 'requestRead');
                    let req = {
                        query: {

                        }
                    };

                    crud.requestRead(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                });
            });

            describe('with query params and desc sort param', () => {
                it('should return array with result field', () => {
                    let spy = jest.spyOn(crud, 'requestRead');
                    let req = {
                        query: {
                            field: "field_value", sort: "-field"
                        }
                    };
                    

                    crud.requestRead(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('result')).toBe(true);
                });
            });

            describe('with query params and sort param not defined direction', () => {
                it('should return array with result field', () => {
                    let spy = jest.spyOn(crud, 'requestRead');
                    let req = {
                        query: {
                            field: "field_value", sort: "field"
                        }
                    };


                    crud.requestRead(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('result')).toBe(true);
                });
            });

            describe('when db return error', () => {
                it('should return array with error field', () => {
                    let spy = jest.spyOn(crud, 'requestRead');
                    let spy_db_select = jest.spyOn(db, 'select').mockImplementation(function (filter, order_by, table_name, callback) {
                        callback('Database error')
                    });

                    let req = {
                        query: {
                            field: "field_value", sort: "-field"
                        }
                    };
                    

                    crud.requestRead(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                    spy.mockRestore();
                    spy_db_select.mockRestore();
                });
            });


        });

        describe('requestAdd', () => {


            describe('with fields in request body', () => {
                it('should return array with result field', () => {
                    let spy = jest.spyOn(crud, 'requestAdd');
                    let req = {
                        body: { field1: 'field_value' }
                    };
                    

                    crud.requestAdd(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('result')).toBe(true);
                });
            });

            describe('with missing request body', () => {
                it('should return array with error field', () => {
                    let spy = jest.spyOn(crud, 'requestAdd');
                    let req = {
                        
                    };

                    
                    
                    
                    crud.requestAdd(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                });
            });

            describe('with missing request body fields', () => {
                it('should return array with error field', () => {
                    let spy = jest.spyOn(crud, 'requestAdd');
                    let req = {
                        body:{

                        }
                    };
                    

                    crud.requestAdd(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                });
            });

            describe('with request body fields that are not contained in table', () => {
                it('should return array with error field', () => {
                    let spy = jest.spyOn(crud, 'requestAdd');
                    let req = {
                        body:{
                            unknown_field: "unknown_value"
                        }
                    };
                    

                    crud.requestAdd(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                });
            });

            describe('when db return error', () => {
                it('should return array with error field', () => {
                    let spy = jest.spyOn(crud, 'requestAdd');
                    let spy_db_insert = jest.spyOn(db,'insert').mockImplementation(function (entries, table_name, callback) {
                        callback('Database error')
                    });
                   

                    let req = {
                        body: { field1: 'field_value' }
                    };
                    
                    
                    crud.requestAdd(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                    spy.mockRestore();
                    spy_db_insert.mockRestore();
                });
            });


        });

        
        describe('requestDelete', () => {

            describe('with provided params key and value', () => {
                it('should return array with result field', () => {
                    req = {
                        params: {
                            id: '1'
                        }
                    };

                    crud.requestDelete(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('result')).toBe(true);
                });
            });
            
            describe('with missing params key and value', () => {
                it('should return array with error field', () => {
                    req = {
                        params: {
                            
                        }
                    };

                    crud.requestDelete(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                });
            });

            describe('when db return error', () => {
                it('should return array with error field', () => {
                    let spy_db_delete = jest.spyOn(db, 'delete').mockImplementation(function (filter, table_name, callback) {
                        callback('Database error')
                    });


                    let req = {
                        params: {
                            id: '1'
                        }
                    };


                    crud.requestDelete(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                    
                    spy_db_delete.mockRestore();
                });
            });
        });

        describe('requestUpdate', () => {

            describe('with provided params key and value', () => {
                it('should return array with result field', () => {
                    req = {
                        params: {
                            id: '1'
                        },
                        body:{
                            field1: 'new_value'
                        }
                    };

                    crud.requestUpdate(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('result')).toBe(true);
                });
            });

            describe('with missing params key and value', () => {
                it('should return array with error field', () => {
                    req = {
                        params: {

                        }
                    };

                    crud.requestUpdate(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                });
            });

            describe('with missing request body', () => {
                it('should return array with error field', () => {
                    req = {
                        params: {
                            id: '1'
                        },
                        
                    };

                    crud.requestUpdate(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                });
            });

            describe('with missing request body fields', () => {
                it('should return array with error field', () => {
                    req = {
                        params: {
                            id: '1'
                        },
                        body:{

                        }
                    };

                    crud.requestUpdate(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                });
            });

            describe('with request body that not contail valid table fields', () => {
                it('should return array with error field', () => {
                    req = {
                        params: {
                            id: '1'
                        },
                        body:{
                            unknown_field: 'unknown_value'
                        }

                    };

                    crud.requestUpdate(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);
                });
            });

            describe('when db return error', () => {
                it('should return array with error field', () => {
                    let spy_db_update = jest.spyOn(db, 'update').mockImplementation(function (filter,updated_entry, table_name, callback) {
                        callback('Database error')
                    });


                    let req = {
                        params: {
                            id: '1'
                        },
                        body: {
                            field1: 'new_value'
                        }
                    };


                    crud.requestUpdate(req, res);
                    expect(res.send).toBeCalled();
                    expect(res.send.mock.calls[0][0].hasOwnProperty('error')).toBe(true);

                    spy_db_update.mockRestore();
                });
            });
        });
        

    });

});
