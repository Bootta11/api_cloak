const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer();
const consumers = require('./consumers.js');

const base_path = '/consumers';

router.get(base_path, consumers.requestRead);
router.get(base_path + '/:id([0-9]+)', consumers.requestRead);
router.post(base_path, consumers.requestAdd);
router.delete(base_path + '/:id([0-9]+)', upload.any(), consumers.requestDelete);
router.delete(base_path + '/:name([a-zA-Z09_]+)', upload.any(), consumers.requestDelete);
router.patch(base_path + '/:id([0-9]+)', upload.any(), consumers.requestUpdate);
router.patch(base_path + '/:name([a-zA-Z09_]+)', upload.any(), consumers.requestUpdate);

module.exports = router;