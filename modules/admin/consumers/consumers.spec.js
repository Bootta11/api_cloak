const consumers = require('./consumers.js');

describe('Admin consumers', () => {

    it('should provide api crud methods', () => {
        expect(typeof consumers.requestAdd).toBe('function');
        expect(typeof consumers.requestRead).toBe('function')
        expect(typeof consumers.requestUpdate).toBe('function')
        expect(typeof consumers.requestDelete).toBe('function')
    });
});