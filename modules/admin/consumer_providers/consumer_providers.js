// admin providers
const config = require('../../../config/admin_app.config.js');
const simplog = require('../../simplog/simplog.js');
const log = new simplog(config.log_level, 'admin_consumer_providers');
const api_crud = require('../../api_crud/api_crud.js');
const db = require('../../database/db.js');

const table_name = 'consumer_providers';
const table_display_name = 'Consumer providers';
const table_fields = ['provider', 'consumer','response_content_type'];

module.exports = api_crud(db,table_name,table_display_name,table_fields,log);