const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer();
const consumer_providers = require('./consumer_providers.js');

let base_path = '/consumer_providers';

router.get(base_path, consumer_providers.requestRead);
router.get(base_path + '/:id([0-9]+)', consumer_providers.requestRead);
router.post(base_path, consumer_providers.requestAdd);
router.delete(base_path + '/:id([0-9]+)', upload.any(), consumer_providers.requestDelete);
router.patch(base_path + '/:id([0-9]+)', upload.any(), consumer_providers.requestUpdate);

module.exports = router;