const consumer_providers = require('./consumer_providers.js');

describe('Admin consumer_providers', () => {
    
    it('should provide api crud methods', () => {
        expect(typeof consumer_providers.requestAdd).toBe('function');
        expect(typeof consumer_providers.requestRead).toBe('function')
        expect(typeof consumer_providers.requestUpdate).toBe('function')
        expect(typeof consumer_providers.requestDelete).toBe('function')
    });
});
