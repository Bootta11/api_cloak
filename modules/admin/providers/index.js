const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer();
const providers = require('./providers.js');

const base_path = '/providers';

router.get(base_path, providers.requestRead);
router.get(base_path + '/:id([0-9]+)', providers.requestRead);
router.post(base_path, providers.requestAdd);
router.delete(base_path + '/:id([0-9]+)', upload.any(), providers.requestDelete);
router.delete(base_path + '/:name([a-zA-Z09_]+)', upload.any(), providers.requestDelete);
router.patch(base_path + '/:id([0-9]+)', upload.any(), providers.requestUpdate);
router.patch(base_path + '/:name([a-zA-Z09_]+)', upload.any(), providers.requestUpdate);

module.exports = router;