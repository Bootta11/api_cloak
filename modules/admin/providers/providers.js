// admin providers
const config = require('../../../config/admin_app.config.js');
const simplog = require('../../simplog/simplog.js');
const log = new simplog(config.log_level, 'admin_providers');
const api_crud = require('../../api_crud/api_crud.js');
const db = require('../../database/db.js');

const table_name = 'providers';
const table_display_name = 'Provider';
const table_fields = ['name', 'url', 'request_type', 'content_type', 'timeout'];

module.exports = api_crud(db,table_name,table_display_name,table_fields,log);