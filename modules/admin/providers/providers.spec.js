const providers = require('./providers.js');

describe('Admin providers', () => {

    it('should provide api crud methods', () => {
        expect(typeof providers.requestAdd).toBe('function');
        expect(typeof providers.requestRead).toBe('function')
        expect(typeof providers.requestUpdate).toBe('function')
        expect(typeof providers.requestDelete).toBe('function')
    });
});