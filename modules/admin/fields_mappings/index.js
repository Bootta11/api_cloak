const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer();
const fields_mappings = require('./fields_mappings.js');

let base_path = '/fields_mappings';

router.get(base_path, fields_mappings.requestRead);
router.get(base_path+'/:id([0-9]+)', fields_mappings.requestRead);
router.post(base_path, fields_mappings.requestAdd);
router.delete(base_path + '/:id([0-9]+)', upload.any(), fields_mappings.requestDelete);
router.patch(base_path + '/:id([0-9]+)', upload.any(), fields_mappings.requestUpdate);

module.exports = router;