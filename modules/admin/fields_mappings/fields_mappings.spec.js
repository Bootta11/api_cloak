const fields_mappings = require('./fields_mappings.js');

describe('Admin fields_mappings', () => {

    it('should provide api crud methods', () => {
        expect(typeof fields_mappings.requestAdd).toBe('function');
        expect(typeof fields_mappings.requestRead).toBe('function')
        expect(typeof fields_mappings.requestUpdate).toBe('function')
        expect(typeof fields_mappings.requestDelete).toBe('function')
    });
});