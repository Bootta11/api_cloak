// admin fields_mappings
const config = require('../../../config/admin_app.config.js');
const simplog = require('../../simplog/simplog.js');
const log = new simplog(config.log_level, 'admin_fields_mappings');
const api_crud = require('../../api_crud/api_crud.js');
const db = require('../../database/db.js');

const table_name = 'fields_mappings';
const table_display_name = 'Fields mappings';
const table_fields = ['field_name', 'mapping', 'type', 'consumer_provider'];

module.exports = api_crud(db,table_name,table_display_name,table_fields,log);