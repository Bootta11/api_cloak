const config = require('../../../config/client_app.config.js');
const simplog = require('../../simplog/Simplog.js');
const log = new simplog(config.log_level, 'consumers');
const db = require('../../database/db.js');

function getData(filter, data, callback) {
    db.select(filter, {}, 'consumers', function (err, consumers) {
        if (err) {
            return callback({msg: err, statusCode: 500});
        }

        if (consumers && consumers.length > 0) {
            let return_data = Object.assign({}, data, {consumer: consumers[0]});
            return callback(null, return_data);
        }

        return callback({msg: 'No consumers with filter: ' + JSON.stringify(filter), statusCode: 401});
    });
}

module.exports = {
    getData: getData
};