const consumers = require('./consumers.js');

describe('Consumers', function () {
    describe('checkApikey function', function () {
        let apikey = 'testtest';
        let error;
        let data;
        
        beforeAll(function (done) {
            consumers.getData({api_key: apikey}, {}, function (e, d) {
                error = e;
                data = d;
                done();

            });
        });

        it('should not return error', function () {
            expect(error).toBeNull();

        });

        it('should return data object', function () {
            expect(typeof data === 'object').toBe(true);
        });
        
        it('should return consumer in data object', function () {
            expect(typeof data.consumer === 'object').toBe(true);
        });

        
        describe('call with filter that not object', () => {
            test('should return error with statusCode 500', (done) => {
                consumers.getData('', {}, function (e, d) {
                    expect(typeof e).toBe('object');
                    expect(e.statusCode).toBe(500);
                    done();

                });
            });
        });

        describe('call with non existing id filter', () => {
            test('should return error with statusCode 401', (done) => {
                consumers.getData({id: -1}, {}, function (e, d) {
                    expect(typeof e).toBe('object');
                    expect(e.statusCode).toBe(401);
                    done();

                });
            });
        });
         
        
        
    });
});