const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer();
const providers = require('./providers.js');

const base_bath = '/providers';

router.all(base_bath + '/:id([0-9]+)/request', upload.any(), providers.requestByProviderId);
router.all(base_bath + '/:name([a-zA-Z0-9_]+)/request', upload.any(), providers.requestByProviderName);

module.exports = router;