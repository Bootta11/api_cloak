const config = require('../../../config/client_app.config.js');
const simplog = require('../../simplog/Simplog.js');
const log = new simplog(config.log_level, 'client_providers');
const async = require('async');
const request = require('request');
const db = require('../../database/db.js');
const consumers = require('../consumers/consumers.js');
const plugins = require('../../plugins/plugins.js');

const installed_plugins = plugins.get();
console.log('Plugins instaled: ', installed_plugins);

function requestByProviderName(req, res) {
    providerRequest(req, res, 'name');
}

function requestByProviderId(req, res) {
    providerRequest(req, res, 'id');
}

function providerRequest(req, res, by) {
    let init_data = {};
    let provider_filter = {};
    if (by === 'id') {
        init_data = {provider: {id: req.params.id}};
        provider_filter = {id: req.params.id};
    } else {
        init_data = {provider: {name: req.params.name}};
        provider_filter = {name: req.params.name};
    }

    let date = new Date();
    let date_part = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    let time_part = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '.' + date.getMilliseconds();
    init_data.creation_date = date_part + ' ' + time_part;

    console.log('Body: ', req.body);
    console.log('Params: ', req.params);
    console.log('Query: ', req.query);

    async.waterfall([
        function getConsumerStep(callback) {
            let apikey = req.get('apikey');
            if(!apikey && req && req.query){
                apikey = req.query.apikey;
            }
            if (!apikey && req && req.body) {
                apikey = req.body.apikey;
                if (!apikey) {
                    return callback({msg: 'Unable get apikey. Access denied.', statusCode: 401});
                }
            }

            consumers.getData({api_key: apikey}, init_data, callback);
        },
        function getProviderStep(data, callback) {
            getProviderData(provider_filter, data, callback);
        },
        function saveTransactionStep(data, callback) {
            saveTransaction(data, callback);
            //callback(null,data);
        },
        function extractAndSaveInputParametersStep(data, callback) {
            data.parameters = Object.assign({}, req.body, req.params, req.query);
            saveInputParameters(data, callback);
            //callback(null,data);
        },
        function checkConsumerProviderStep(data, callback) {
            //console.log('data before check consumer: ', data);
            checkConsumerProvider({consumer: data.consumer.id, provider: data.provider.id}, data, callback);
        },
        function getConsumerProviderPluginsStep(data, callback) {
            getConsumerProviderPlugins({consumer_provider: data.consumer_provider.id}, data, callback);
        },
        function runInitPluginsStep(data, callback) {
            runPlugins('init', data, callback);
        },
        function prepareInputParametersStep(data, callback) {

            console.log('PARAMETERS: ', data.parameters);
            prepareInputParameters({consumer_provider: data.consumer_provider.id}, data, callback);

        },
        function runPostInputPluginsStep(data, callback) {
            runPlugins('post_input', data, callback);
        },
        function callProviderUrlStep(data, callback) {

            callProviderUrl(data, callback);
//            data['response_data'] = {raw_body: '{"var1":"val1","var2":"val2"}'};
//            console.log('DDD: ', data);
//            callback(null, data);
        },
        function runPostCallProviderPluginsStep(data, callback) {
            runPlugins('post_call_provider', data, callback);
        },
        function parseReponseBody(data, callback) {
            //console.log('data before parse raw body: ', data);
            parseResponseBody(data, callback);
        },
        function runPostParseReponsePluginsStep(data, callback) {
            runPlugins('post_parse_response', data, callback);
        },
        function saveResponseParametersStep(data, callback) {
            saveResponseParameters(data, callback);
            //callback(null,data);
        },
        function runPostSaveResponseParametersPluginsStep(data, callback) {
            runPlugins('post_save_response_parameters', data, callback);
        },
        function prepareOutputParametersStep(data, callback) {
            prepareOutputParameters({consumer_provider: data.consumer_provider.id}, data, callback);
        },
        function runPostPrepareOutputParametersPluginsStep(data, callback) {
            runPlugins('post_prepare_output', data, callback);
        },
        function runFinishPluginsStep(data, callback) {
            runPlugins('finish', data, callback);
        }
    ], function (err, result) {
        if (err && err.msg && err.statusCode) {
            res.status(err.statusCode).send(err.msg);
        } else if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).send(result.response_data.output);
        }
    });
}

function requestAddProvider(req, res) {
    //console.log(res.body);
    if (!(req.body.name && req.body.url && req.body.request_type && req.body.content_type && req.body.timeout)) {
        res.status(400).send('Request missing parameters');
    }
    db.insert([req.body], 'providers', function (err, result) {
        console.log('err: ', err, ' result: ', result);
        res.send('ok');
    });

}

function saveTransaction(data, callback) {
    //console.log('data before saving transaction: ', data);


    db.insert([{creation_date: data.creation_date, consumer: data.consumer.id, provider: data.provider.id}], 'transactions', function (err, result) {
        console.log('err: ', err, 'Inserted transaction: ', result);
        if (err) {
            return callback(err);
        }

        data['transaction'] = result[0];
        callback(null, data);
    });
}

function saveInputParameters(data, callback) {
    let paramters_entries_array = [];
    for (let key in data.parameters) {
        let parameter_entry = {transaction: data.transaction.id, field_name: key, field_value: data.parameters[key]};
        paramters_entries_array.push(parameter_entry);

    }
    db.insert(paramters_entries_array, 'consumer_transaction_fields', function (err, result) {
        console.log('err: ', err, ' data: ', data);
        callback(null, data);
    });
}

function getProviderData(filter, data, callback) {
    db.select(filter, {}, 'providers', function (err, providers) {
        if (err) {
            return callback({msg: err, statusCode: 500});
        }

        if (providers && providers.length > 0) {
            let return_data = Object.assign({}, data, {provider: providers[0]});
            return callback(null, return_data);
        }

        return callback({msg: 'No providers with: ' + JSON.stringify(filter), statusCode: 404});
    });
}

function checkConsumerProvider(filter, data, callback) {
    db.select(filter, {}, 'consumer_providers', function (err, consumer_providers) {
        if (err) {
            return callback({msg: err, statusCode: 500});
        }
        if (consumer_providers && consumer_providers.length > 0) {
            let return_data = Object.assign({}, data, {consumer_provider: consumer_providers[0]});
            callback(null, return_data);
        } else {
            return callback({msg: 'No providers for consumer: ' + filter.consumer.id, statusCode: 401});
        }
    });
}

function getConsumerProviderPlugins(filter, data, callback) {
    console.log('Filter plugins: ', filter);
    db.select(filter, {priority: 'DESC'}, 'consumer_provider_plugins', function (err, consumer_provider_plugins) {
        if (err) {
            return callback({msg: err, statusCode: 500});
        }

        console.log('consumer_provider_plugins from db: ', consumer_provider_plugins)
        if (consumer_provider_plugins && consumer_provider_plugins.length > 0) {
            let sorted_plugins = {};
            for (let i = 0; i < consumer_provider_plugins.length; i++) {
                let plugin_phase = consumer_provider_plugins[i].phase;
                if (!sorted_plugins.hasOwnProperty(plugin_phase)) {
                    sorted_plugins[plugin_phase] = [];
                }
                sorted_plugins[plugin_phase].push(consumer_provider_plugins[i]);
            }
            console.log('sorted plugins: ', sorted_plugins)
            let return_data = Object.assign({}, data, {consumer_provider_plugins: sorted_plugins});
            console.log('consumer provider plugins return data: ', data.return_data)
            callback(null, return_data);
        } else {
            let return_data = Object.assign({}, data, {consumer_provider_plugins: []});
            callback(null, return_data);
        }
    });

}


function runPlugins(phase, data, callback) {
    if (!data.consumer_provider_plugins[phase] || (data.consumer_provider_plugins[phase] && data.consumer_provider_plugins[phase].length < 1)) {
        return callback(null, data);
    }

    let plugins_data = data.consumer_provider_plugins[phase];
    let count = plugins_data.length;
    let data_from_plugins = [];
    let plugins_actions = [];

    if (plugins_data.length > 0) {
        for (let i = 0; i < count; i++) {

            let plugin_name = plugins_data[i].name;
            let plugin_settings = {};
            try {
                plugin_settings = JSON.parse(plugins_data[i].settings);
            } catch (ex) {
                log.error('Unable to parse plugin settings: ', plugins_data[i].settings);
            }
            let plugin = Object.assign({}, installed_plugins[plugins_data[i].name]);
            plugin.load(plugin_settings);
            plugins_actions.push(function (data, done) {
                plugin.action(data, function (plugin_error, plugin_return_data) {
                    if (plugin_error) {
                        log.error(plugin_error);
                    }
                    done(plugin_error, plugin_return_data);
                });
            });

        }
        
        let run_plugins = async.compose.apply(null, plugins_actions);  //async.compose execute fucntions in reversed order
        run_plugins(data, function (err, data) {
            if (err) {
                log.error(err);
            }
            callback(null, data);
        });
    } else {
        callback(null, data);
    }
}



function prepareInputParameters(filter, data, callback) {
    db.select(Object.assign({}, filter, {type: 'in'}), {}, 'fields_mappings', function (err, mappings) {
        if (err) {
            return callback({msg: err, statusCode: 500});
        }

        console.log('-------> Data: ', data);
        let request_data = data.parameters;
        let is_strinct_mapping = false;
        if(typeof data.provider.strict_mapping !== 'undefined' && data.provider.strict_mapping == true){
            request_data = {};
            is_strinct_mapping = true;
        }

        if (mappings && mappings.length > 0) {

            request_data = {};
            for (let parameter_key in data.parameters) {
                for (let i = 0; i < mappings.length; i++) {
                    if (mappings[i].field_name === parameter_key) {
                        request_data[mappings[i].mapping] = data.parameters[parameter_key];
                        continue;
                    }
                    if(!is_strinct_mapping){
                        request_data[parameter_key] = data.parameters[parameter_key];
                    }
                }
            }

            // if(typeof data.provider.strict_mapping !== 'undefined' && data.provider.strict_mapping == false){
            //     return_data = Object.assign({}, data, {request_data: request_data});
            // }
        }

        return_data = Object.assign({}, data, {request_data: request_data});

        return callback(null, return_data);
    });
}

function prepareOutputParameters(filter, data, callback) {
    db.select(Object.assign({}, filter, {type: 'out'}), {}, 'fields_mappings', function (err, mappings) {
        if (err) {
            return callback({msg: err, statusCode: 500});
        }

        let return_data = {};
        let response_data = data.response_data;
        let is_strinct_mapping = false;
        if(typeof data.provider.strict_mapping !== 'undefined' && data.provider.strict_mapping == true){
            request_data = {};
            is_strinct_mapping = true;
        }

        if (mappings && mappings.length > 0) {
            response_data.output = {};
            for (let parameter_key in response_data.output_body) {
                let out_key = parameter_key;
                for (let i = 0; i < mappings.length; i++) {
                    if (mappings[i].field_name === parameter_key) {
                        out_key = mappings[i].mapping;
                        response_data.output[out_key] = response_data.output_body[parameter_key];
                        continue;
                    }
                    if(!is_strinct_mapping){
                        response_data.output[out_key] = response_data.output_body[parameter_key];
                    }
                }

            }

            return_data = Object.assign({}, data, {response_data: response_data});
        } else {
            if(is_strinct_mapping){
                response_data.output = {};
                return_data = Object.assign({}, data, {response_data: response_data});
            }else{
                response_data.output = response_data.output_body;
                return_data = Object.assign({}, data, {response_data: response_data});
            }
        }
        //console.log('response data body: ', response_data.body);
        return callback(null, return_data);
    });
}

function callProviderUrl(data, callback) {
    let content_type = data.provider.content_type;

    let request_config = {
        rejectUnauthorized: false,
        requestCert: true,
        agent: false,
        timeout: (data.provider.timeout ? data.provider.timeout : 20000)
    };

    if (content_type === 'application/x-www-form-urlencoded') {
        request_config = Object.assign({}, request_config, {
            method: data.provider.request_type,
            uri: data.provider.url,
            form: data.request_data
        });
    } else if (content_type === 'application/json') {
        request_config = Object.assign({}, request_config, {
            method: data.provider.request_type,
            uri: data.provider.url,
            json: data.request_data
        });

    } else if (content_type === 'multipart/form-data') {
        request_config = Object.assign({}, request_config, {
            method: data.provider.request_type,
            uri: data.provider.url,
            formData: data.request_data
        });

    } else {
        request_config = Object.assign({}, request_config, {
            method: data.provider.request_type,
            uri: data.provider.url,
            multipart: [{
                    'content-type': content_type,
                    body: JSON.stringify(data.request_data)
                }]
        });
    }

    request(request_config, function (err, resp, body) {
        let return_data = Object.assign({}, data, {response_data: {data: resp, raw_body: body}});
        callback(null, return_data);
    });

}

function parseResponseBody(data, callback) {
    let output_body = data.response_data.raw_body;
    try {
        if (data.consumer_provider.response_content_type === 'application/json') {
            output_body = JSON.parse(output_body);
        } else {
            output_body = {raw_response: output_body};
        }
    } catch (err) {
        log.error(err);
        return callback({msg: 'Unable to parse provider response', statusCode: 502});
    }

    data.response_data.output_body = output_body;

    return callback(null, data);
}

function saveResponseParameters(data, callback) {
    let paramters_entries_array = [];

    for (let key in data.response_data.output_body) {
        let parameter_entry = {transaction: data.transaction.id, field_name: key, field_value: data.response_data.output_body[key]};
        paramters_entries_array.push(parameter_entry);
    }

    db.insert(paramters_entries_array, 'provider_transaction_fields', function (err, result) {
        console.log('err: ', err, ' inserted resp params data: ', result);
        callback(null, data);
    });

}



module.exports = {
    requestByProviderId: requestByProviderId,
    requestByProviderName: requestByProviderName,
    requestAddProvider: requestAddProvider
};