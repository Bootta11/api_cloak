const db_wrapper = require('./database_wrapper.js')('postgres');
const db_config = require('../../config/postgres.db.config.js');
const simplog = require('../simplog/Simplog.js');
const log = new simplog(db_config.log_level, 'postgres_db_js');
var pgp = require('pg-promise')();
var db = pgp('postgres://' + db_config.username + ':' + db_config.password + '@' + db_config.host + ':' + db_config.port + '/' + db_config.db_name);

module.exports = function () {
    return Object.assign({}, db_wrapper, {
        executeDbCheck: function (callback) {
            db.connect()
                .then(obj => {
                    callback(null);
                    return obj.done(); // success, release the connection; 
                })
                .catch(error => {
                    return callback('Unable connect to postgres database');
                });
        },
        executeQuery: function (query, callback) {
            db.any(query)
                .then(function (data) {
                    callback(null, data);
                })
                .catch(function (error) {
                    callback(error);
                });
        },
        executeInsert: function (entries, address, done) {
            let entry_value_names = '';
            let comma = '';
            for (let key in entries[0]) {
                entry_value_names += comma + ' ' + key;
                comma = ',';
            }

            let entry_values = '';
            let comma_entries = '';
            let count = 1;
            let values = [];
            for (let entry_key in entries) {

                entry_values += comma_entries + '(';
                let comma_entry = '';
                for (let key in entries[entry_key]) {
                    entry_values += comma_entry + " $" + count++ + "";
                    values.push(entries[entry_key][key]);
                    comma_entry = ',';
                }
                entry_values += ')';
                comma_entries = ', ';
            }

            let insert_query = "INSERT INTO " + address + "(" + entry_value_names + ") values" + entry_values + " returning id";
            console.log('insert query: ', insert_query);

            db.any({
                text: insert_query,
                values: values
            })
                .then(function (data) {
                    done(null, data);
                })
                .catch(function (error) {
                    done(error, false);
                });
        },
        
        executeSelect: function (filter, order_by, address, callback) {
            console.log('select table: ', address);
            let where_filter = ' ';
            let and = '';
            let where = ' WHERE ';
            for (let key in filter) {
                where_filter += where + and + key + " = '" + filter[key] + "'";
                and = ' and ';
                where = '';
            }
            let order_by_part = '';
            if (Object.keys(order_by).length > 0) {
                let order_key = Object.keys(order_by)[0];
                order_by_part = ' ORDER BY ' + order_key + ' ' + order_by[order_key];
            }

            let query = 'SELECT * FROM ' + address + where_filter + order_by_part;
            console.log('select query: ', query);

            db.any(query)
                .then(function (data) {
                    callback(null, data);
                })
                .catch(function (error) {
                    log.error(error);
                    callback(error);
                });
        },
        executeDelete: function (filter, address, callback) {

            let where_filter = '';
            let and = '';
            for (let key in filter) {
                where_filter += and + key + " = '" + filter[key] + "'";
                and = ' and ';
            }

            let query = 'DELETE FROM ' + address + ' WHERE ' + where_filter;

            db.result(query)
                .then(function (result) {
                    callback(null, result.rowCount > 0);
                })
                .catch(function (error) {
                    log.error(error);
                    callback(null, false);
                });
        },
        executeUpdate: function (filter, entry, address, callback) {

            let where_filter = '';
            let and = '';
            for (let key in filter) {
                where_filter += and + key + " = '" + filter[key] + "'";
                and = ' and ';
            }

            let set_values = '';
            let comma = '';
            for (let key in entry) {
                set_values += comma + key + " = '" + entry[key] + "'";
                comma = ', ';
            }

            let update_query = 'UPDATE ' + address + ' SET ' + set_values + ' WHERE ' + where_filter;
            console.log('update: ', update_query);

            db.result(update_query)
                .then(function (result) {
                    callback(null, result.rowCount > 0);
                })
                .catch(function (error) {
                    log.error(error);
                    callback(null, false);
                });
        }
    });
};