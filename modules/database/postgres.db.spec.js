
jest.mock('pg-promise', () => {
    return function () {
        return function (str) {
            return {
                connect: jest.fn().mockImplementationOnce(() => {
                    return new Promise((resolve, reject) => {
                        resolve({
                            done: jest.fn()
                        })
                    })
                }).mockImplementationOnce(() => {
                    return new Promise((resolve, reject) => {
                        reject('Database error');
                    })
                }),
                any: jest.fn().mockImplementationOnce((query) => {
                    return new Promise((resolve, reject) => { // query success
                        resolve({})
                    })
                }).mockImplementationOnce((query) => { //query error
                    return new Promise((resolve, reject) => {
                        reject('Database query error');
                    })
                }).mockImplementationOnce((insert_params) => { //insert success
                    return new Promise((resolve, reject) => {
                        resolve(null, {});
                    })
                }).mockImplementationOnce((insert_params) => { //insert error
                    return new Promise((resolve, reject) => {
                        reject('Unable to insert to database');
                    })
                }).mockImplementationOnce((query) => { //select success
                    return new Promise((resolve, reject) => {
                        resolve(null, []);
                    })
                }).mockImplementationOnce((query) => { //select error
                    return new Promise((resolve, reject) => {
                        reject('Unable to insert to database');
                    })
                })

            }
        }
    }
});

const postgres = require('./postgres.db.js')();


describe('postgres.db', () => {

    describe('method', () => {

        describe('executeDbCheck()', () => {

            describe('if database is available', () => {

                test('should return no error', (done) => {
                    postgres.executeDbCheck((err) => {
                        expect(err).toBe(null);
                        done();
                    })

                });

            });

            describe('if database is not available', () => {

                test('should return error', (done) => {

                    postgres.executeDbCheck((err) => {
                        expect(err).not.toBe(null);
                        done();
                    })

                });

            });

        });

        describe('executeQuery()', () => {

            describe('if query is successfull', () => {

                test('should return no error', (done) => {
                    postgres.executeQuery('query', (err, data) => {
                        expect(err).toBe(null);
                        done();
                    })

                });

            });

            describe('if query is not successfull', () => {

                test('should return error', (done) => {

                    postgres.executeQuery('query', (err, data) => {
                        expect(err).not.toBe(null);
                        done();
                    })

                });

            });

        });

        describe('executeInsert()', () => {

            describe('if insert is successfull', () => {

                test('should return no error', (done) => {
                    postgres.executeInsert([{ name: 'entry1' }], 'some_table', (err, data) => {
                        console.log('insert err: ', err, ' data: ', data);
                        expect(err).toBe(null);
                        expect(data).not.toBe(false);
                        done();
                    })

                });

            });

            describe('if insert is not successfull', () => {

                test('should return error', (done) => {

                    postgres.executeInsert([{ name: 'entry1' }], 'some_table', (err, data) => {
                        expect(err).not.toBe(null);
                        expect(data).toBe(false);
                        done();
                    })

                });

            });

        });

        describe('executeSelect()', () => {

            describe('if select is successfull', () => {

                test('should return no error', (done) => {
                    postgres.executeSelect({ 'field1': 'some_value' }, { 'field1': 'asc' }, 'some_table', (err, data) => {
                        console.log('select err: ', err, ' data: ', data);
                        expect(err).toBe(null);
                        expect(data).not.toBe(false);
                        done();
                    })

                });

            });

            describe('if select is not successfull', () => {

                test('should return error', (done) => {

                    postgres.executeSelect({ 'field1': 'some_value' }, { 'field1': 'asc' }, 'some_table', (err, data) => {
                        expect(err).not.toBe(null);
                        done();
                    })

                });

            });

        });

    });

});
