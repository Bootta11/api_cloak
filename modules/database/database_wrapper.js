
module.exports = function (db_name) {
    if (!db_name) {
        throw new Error('On defining wrapper must set database wrapper name');
    }

    let database_name = db_name;
    let object_composition = {
        db_available: false,
        isAvailable: isAvailable,
        name: getDbName,
        runDbCheck: runDbCheck,
        executeDbCheck: executeDbCheck,
        query: runQuery,
        executeQuery: executeQuery,
        insert: runInsert,
        executeInsert: executeInsert,
        select: runSelect,
        executeSelect: executeSelect,
        delete: runDelete,
        executeDelete: executeDelete,
        update: runUpdate,
        executeUpdate: executeUpdate,
    }


    function getDbName() {
        return database_name;
    }

    function isAvailable() {
        return object_composition.db_available;
    }

    function executeDbCheck(callback) {
        callback('executeDbCheck() function not defined');
    }

    function executeQuery(query, callback) {
        callback('executeQuery() function not defined');
    }

    function executeInsert(entry, address, callback) {
        callback('executeInsert() function not defined');
    }

    function executeSelect(filter, order_by, address, callback) {
        callback('executeSelect() function not defined');
    }

    function executeDelete(filter, address, callback) {
        callback('executeDelete() function not defined');
    }

    function executeUpdate(filter, entry, address, callback) {
        callback('executeUpdate() function not defined');
    }

    function runDbCheck(callback) {
        if (object_composition.db_available) {
            return callback(null);
        } else {
            this.executeDbCheck(function (err) {
                if (!err) {
                    object_composition.db_available = true;
                    return callback(null);
                } else {
                    return callback(err);
                }
            });
        }
    }

    function runQuery(query, callback) {
        this.executeQuery(query, function (err, result) {
            if (!err) {
                if (result.constructor === Array) {
                    return callback(null, result);
                }
                return callback('Error: executeQuery need to return Array');
            }
            return callback(err);
        });



    }

    function runInsert(entry, address, callback) {

        if (typeof entry !== 'object') {
            return callback('Entry parameter must be valid object');
        }


        this.executeInsert(entry, address, function (err, result) {
            if (!err) {
                if (typeof result === 'object') {
                    return callback(null, result);
                }
                return callback('executeInsert need to return object');
            }
            return callback(err);
        });
    }


    function runSelect(filter, order_by, address, callback) {

        if (!filter || typeof filter !== 'object') {
            return callback('Filter parameter must be valid object');
        }

        this.executeSelect(filter, order_by, address, function (err, result) {
            if (!err) {
                if (result.constructor === Array) {
                    return callback(null, result);
                }
                return callback('Error: executeQuery need to return Array');

            }

            return callback(err);
        });
    }

    function runDelete(filter, address, callback) {

        if (!filter || typeof filter !== 'object') {
            return callback('Filter parameter must be valid object');
        }

        this.executeDelete(filter, address, function (err, result) {
            if (!err) {
                if (typeof result === 'boolean') {
                    return callback(null, result);
                }
                return callback('Error: executeDelete need to return Boolean');
            }
            return callback(err);
        });
    }

    function runUpdate(filter, entry, address, callback) {

        if (!filter || typeof filter !== 'object') {
            return callback('Filter parameter must be valid object');
        }


        if (!entry || typeof entry !== 'object') {
            return callback('Entry parameter must be valid object');
        }


        this.executeUpdate(filter, entry, address, function (err, result) {
            if (!err) {
                if (typeof result === 'boolean') {
                    return callback(null, result);
                }
                return callback('Error: executeUpdate need to return Boolean');
            }
            return callback(err);
        });
    }

    return object_composition;
};

