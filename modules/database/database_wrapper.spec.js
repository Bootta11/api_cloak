
const database_wrapper = require('./database_wrapper.js');

describe('Database wrapper', () => {
    const db_name = 'some_db';
    describe('creation', () => {
        describe('with database name', () => {
            it('should return object without error', () => {
                let err;
                let db;
                try {
                    db = database_wrapper(db_name);
                } catch (error) {

                    err = error;
                }
                expect(err).toBe(undefined);
                expect(typeof db).toBe('object');
            });
        });
        describe('without database name', () => {
            it('should return error', () => {
                let err;
                try {
                    database_wrapper();
                } catch (error) {
                    err = error;
                }
                expect(err instanceof Error).toBe(true);
            });
        });
    });


    describe('method', () => {
        let db;

        beforeEach(function (done) {
            db = Object.assign({}, database_wrapper(db_name));

            done();
        });


        describe('name()', () => {
            it('should return database name', () => {
                expect(db.name()).toBe(db_name);
            });
        });

        describe('name()', () => {
            it('should return database name', () => {
                expect(db.name()).toBe(db_name);
            });
        });


        describe('runDbCheck()', () => {

            describe('with executeDbCheck not defined', () => {

                it('should return error', () => {
                    db.runDbCheck(function (err) {
                        expect(err != null).toBe(true);
                    });
                });

            });

            describe('with executeDbCheck defined', () => {

                it('should return no errors', () => {
                    db.executeDbCheck = function (callback) {
                        return callback(null, true);
                    }

                    db.runDbCheck(function (err) {
                        expect(err).toBeNull();
                        expect(db.isAvailable()).toBe(true);
                    });

                    db.runDbCheck(function (err) {
                        expect(err).toBeNull();
                    });
                });

            });
        });

        describe('query()', () => {

            describe('with executeQuery not defined', () => {

                it('should return error', () => {
                    db.query('some query',function (err) {
                        expect(err != null).toBe(true);
                    });
                });

            });

            describe('with executeQuery defined', () => {

                it('should return no errors and return array', () => {
                    db.executeQuery = function (query,callback) {
                        return callback(null, []);
                    }

                    db.query('some query',function (err,rows) {
                        expect(err).toBeNull();
                        expect(rows instanceof Array).toBe(true);
                    });   
                });

                it('should return error if not returned resulat as array', () => {
                    db.executeQuery = function (query, callback) {
                        return callback(null, {});
                    }

                    db.query('some query', function (err, rows) {
                        expect(err).toEqual(expect.anything());
                    });
                });

            });
        });

        describe('insert()', () => {

            describe('with executeInsert not defined', () => {

                it('should return error', () => {
                    db.insert({},'some_table', function (err) {
                        expect(err != null).toBe(true);
                    });
                });

            });

            describe('with executeQuery defined', () => {

                it('should return error if entry is not type of object', () => {
                    
                    db.insert('entry','some_table', function (err, rows) {
                        expect(err).toEqual(expect.anything());
                    });
                });

                it('should return error if not returned object from executeInsert function', () => {
                    db.executeInsert = function (entry, address, callback) {
                        return callback(null, 'returned string');
                    }

                    db.insert({}, 'some_table', function (err, rows) {
                        expect(err).toEqual(expect.anything());
                    });
                });

                it('should return no error if returned callback with object and without errors', () => {
                    db.executeInsert = function (entry, address, callback) {
                        return callback(null, {});
                    }

                    db.insert({}, 'some_table', function (err, inserts) {
                        expect(err).toBeNull();
                        expect(typeof inserts).toBe('object');
                    });
                });

                
                

            });
        });

        describe('select()', () => {

            describe('with executeSelect not defined', () => {

                it('should return error', () => {
                    db.select({},'asc','some_table', function (err) {
                        expect(err != null).toBe(true);
                    });
                });

            });

            describe('with executeSelect defined', () => {

                it('should return no errors and return array', () => {
                    db.executeSelect = function (filter,order_by,address, callback) {
                        return callback(null, []);
                    }

                    db.select({},'asc','some_table', function (err, rows) {
                        expect(err).toBeNull();
                        expect(rows instanceof Array).toBe(true);
                    });
                });

                it('should return error if not returned resulat as array', () => {
                    db.executeSelect = function (filter,order_by,address, callback) {
                        return callback(null, {});
                    }

                    db.select({}, 'asc','some_table', function (err, rows) {
                        expect(err).toEqual(expect.anything());
                    });
                });

            });
        });

        describe('delete()', () => {

            describe('with executeDelete not defined', () => {

                it('should return error', () => {
                    db.delete({}, 'some_table', function (err) {
                        expect(err != null).toBe(true);
                    });
                });

            });

            describe('with executeDelete defined', () => {

                it('should return no errors and return boolean', () => {
                    db.executeDelete = function (filter, address, callback) {
                        return callback(null, true);
                    }

                    db.delete({}, 'some_table', function (err, delete_result) {
                        expect(err).toBeNull();
                        expect(typeof delete_result).toBe('boolean');
                    });
                });

                it('should return error if not returned resulat as boolean', () => {
                    db.executeDelete = function (filter, address, callback) {
                        return callback(null, {});
                    }

                    db.delete({}, 'some_table', function (err, delete_result) {
                        expect(err).toEqual(expect.anything());
                    });
                });

                it('should return error if not filter type of object', () => {
                    db.executeDelete = function (filter, address, callback) {
                        return callback(null, true);
                    }

                    db.delete('wrong_filter', 'some_table', function (err, delete_result) {
                        expect(err).toEqual(expect.anything());
                    });
                });

            });
        });

        describe('update()', () => {

            describe('with executeUpdate not defined', () => {

                it('should return error', () => {
                    db.update({},{}, 'some_table', function (err) {
                        expect(err != null).toBe(true);
                    });
                });

            });

            describe('with executeUpdate defined', () => {

                it('should return no errors and return boolean', () => {
                    db.executeUpdate = function (filter, entry, address, callback) {
                        return callback(null, true);
                    }

                    db.update({},{}, 'some_table', function (err, update_result) {
                        expect(err).toBeNull();
                        expect(typeof update_result).toBe('boolean');
                    });
                });

                it('should return error if not returned resulat as boolean', () => {
                    db.executeUpdate = function (filter, entry, address, callback) {
                        return callback(null, {});
                    }

                    db.update({},{}, 'some_table', function (err, update_result) {
                        expect(err).toEqual(expect.anything());
                    });
                });

                it('should return error if not filter type of object', () => {
                    db.executeUpdate = function (filter, entry, address, callback) {
                        return callback(null, true);
                    }

                    db.update('wrong_filter',{}, 'some_table', function (err, update_result) {
                        expect(err).toEqual(expect.anything());
                    });
                });

                it('should return error if not entry type of object', () => {
                    db.executeUpdate = function (filter, entry, address, callback) {
                        return callback(null, true);
                    }

                    db.update({},'wrong_filter', 'some_table', function (err, update_result) {
                        expect(err).toEqual(expect.anything());
                    });
                });

            });
        });

    });

});